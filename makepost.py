#!/usr/bin/env python3

import sys
from datetime import datetime

template = """
{title}
{underline}
:data: {year}-{month:02d}-{day:02d} {hour}:{minute:02d}
:category: {category}
:tags: {tags}
:slug: {slug}
:status: draft
"""

if len(sys.argv) < 3:
    print("\nUsage: makepost.py title category [tag1 tags2 tagN]")
    sys.exit("no title, catergory or tags given")

title = sys.argv[1]
category = sys.argv[2]
tags = (", ").join(sys.argv[3:])

slugstr = title.strip().lower()

reps = {' ':'_', 'ä': 'ae', 'ö':'oe', 'ü': 'ue', 'ß': 'ss'}

for orig, rep in reps.items():
    slugstr = slugstr.replace(orig, rep)

now = datetime.today()

filename="content/{:04d}-{:02d}-{:02d}_{}.rst".format(now.year, now.month, now.day, slugstr)

slug = "{:04d}/{}".format(now.year, slugstr)

template = template.strip().format(title=title, underline='#'*len(title), year=now.year, month=now.month, day=now.day, hour=now.hour, minute=now.minute, category=category, tags=tags, slug=slug)
template += "\n\n\n"
print(template)

print("assume to be saved as "+filename)

with open(filename, "w") as postfile:
    postfile.write(template)
print(filename + " written")
