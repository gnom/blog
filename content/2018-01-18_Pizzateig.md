Title: Pizza vom Grill
Date: 2018-01-18 14:24
Category: kochen
Tags: kochen, pizza, teig, grill
Slug: 2018/pizza
Status: published
Author: telegnom

![Pizza auf dem Stein, fast fertig]({filename}/images/2018/pizza_auf_dem_stein.jpg)

Nach dem ich heute mal wieder im HomeOffice saß und mir nach einem schnellen (wenig manueller Aufwand) Mittagessen zu mute war, habe ich mich mal wieder an selbstgemachter Pizza versucht. 

Der Teig ist ein einfacher, halbfester Hefeteig mit etwas Olivenöl für den Geschmack. Da ich eine Küchenmaschine habe, die mir das Kneten abnimmt ist der zeitliche Aufwand recht überschaubar.

Wenn man die Pizzen auf dem Grill zubereiten möchte, dann ist es sinnvoll als erstes den Grill anzuwerfen und den Pizzastein aufzulegen, damit dieser durch und durch heiß werden kann.

## Zutaten

- 260ml Wasser (lauwarm)
- 21g frische Hefe (entsprechend ½ Päckchen Trockenhefe)
- 6g Zucker
- 500g Mehl Typ 550
- 50g Olivenöl
- 12g Salz

## Zubereitung

1. Hefe und Zucker in Wasser auflösen. Alle anderen Zutaten  dazu geben und mindesten 10 Minuten kneten. Der Teig darf nicht mehr klebrig sein.

1. Den Teig 20 Minuten rasten lassen.

1. Teig ausrollen und Pizzen formen.

1. Pizzen nach  Lust und Laune belegen.

1. Pizza auf dem Pizzastein ca. 5 Minuten backen. Sie ist fertig sobald der Käse komplett geschmolzen ist und der Rand anfängt zu bräunen. Im vorgeheiztem Backofen braucht die Pizza bei 225°C ca.5-8 Minuten.

1. Die Pizza dann noch mit gehacktem (ideal ist frisches) Basilikum bestreuen.


Dann viel Freude beim Nachkochen und guten Hunger!

