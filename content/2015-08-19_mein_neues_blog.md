Title: Mein neues Blog
Date: 2015-08-19 18:45
Category: mixed
Tags: blog, pelican, python, wordpress
Slug: 2015/mein_neues_blog
Status: published

Ich habe mir nun endlich die Zeit genommen, mein Blog mal von grundauf neu aufzusetzen.
Dabei ging es mir nicht nur um eine neue, frische Wordpressinstallation, sondern
um einen kompletten Neuanfang mit einer anderen Technik unter der Haube. wordpress
hat für miche eigentlich ganz gut funktioniert, aber ich war auch schon mal Opfer
einer Sicherheitslücke in Wordpress. Der Schaden war überschaubar und ich hatte
ein Backup von den Daten, aber ärgerlich war es und es hat auch einiges an Zeit
gekostet.

## Die neue Technik
Nach dem ich nun ja von Wordpress weg wollte, war ich auf der Suche nach anderen
Lösungen. Noch Mitten in der Suche bin ich dann in der [Python UG Frankfurt](http://ccc-ffm.de/2015/04/monatlicher-python-user-group-treff-im-hackquarter/)
über [Pelican](https://getpelican.com) gestolpert. Pelican ist ein Tool zum Erstellen
statischer Seiten. Dabei werden die Seiten in Markdown oder ReStructured Text
geschrieben und dann einmal in HTML übersetzt. Die so entstehende Seite ist
komplett statisch, was auch einige Nachteile mitsich bringt.

## Handlungsbedarf
Ganz zufrieden bin ich mit dem jetzigen Setup noch nicht. Im wesentlichen sind
noch drei Punkte offen, die ich noch angehen will:

1. Das Theme: Geht zwar, könnte aber besser sein
2. Einbinden und skalieren von Bildern: Mache ich derzeit noch zu Fuß, das muss irgendwie automagisch geschehen
3. Automatische "Neuerstellung" der Seiten nach einem Commit: Hier sollte ein CommitHook helfen können

## Vor- und Nachteile
Die Vorteile sind eine statische Seite, die - außer dem Webserver - serverseitig
keine weiteren Abhänigkeiten hat. Ein Nachteil ist systembedingt nunmal, dass es
keine Kommentarfunktionen oder ähnliches geben kann, da solche Funktionen serverseitig
ausgeführten Code erfordern.

## ready… set… GO!
Auch wenn ich noch nicht alle Probleme gelößt habe, so will ich dennoch schon mit
dem neuen Blog online. Sonst kümmere ich mich einfach nie darum. Also GO!

Das alte Blog findet sich übrigens (als statisches Archiv) [hier](http://old.telegnom.org)
