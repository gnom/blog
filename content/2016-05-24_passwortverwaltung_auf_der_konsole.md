Title: Passwortverwaltung auf der Konsole
Date: 2016-05-24 16:38
Category: linux
Tags: passwortmanager, pass, gpg, pgp, verschlüsselung, passwörter
Slug: 2016/passwortverwaltung_auf_der_konsole
Status: published
Authors: telegnom

Lange Zeit habe ich den Passwortmanager *KeePassX* benutzt, war aber nie so richtig zufriden mit dem Programm. Ja, es tut was es soll. Es speichert die Passwörter in einer verschlüsselten Datenbank die man mittels Passwort oder Keyfile vor dem Zugriff durch Dritte schützt. Allerdings ist das Programm ein ziemliches Monstrum und benötigt Unmengen an Abhängigkeiten um zu funktionieren. So, z.B. Mono, da es sich eigentlich™ um eine .NET-Anwendung für Windows handelt.

Daher habe ich mich schon vor einiger Zeit nach einer Alternative umgesehen und mit bei dem Komondozeilentool [*pass*](https://www.passwordstore.org) gelandet. Das Programm speichert die Passwörter in einer Ordernerstruktur, jedes Passwort in einer eigenen kleinen Datei, die gpg-verschlüsslt ist.
```
/
|
|- zweig 1
|  |- eintrag 1
|- zweig 2
|  |- zweig 2a
|  |  |- eintrag 2a1
|  |  |- eintrag 2a2
|  |- eintrag 2b
|- zweig 3
   |- eintrag 3
```

Daher lassen sich die Passwörter leicht in einem git verwalten und über den heimischen "Server" zwischen den verschiedenen Geräten synchronisieren.

Mindestens für Android Smartphones gibt es eine [App](https://github.com/zeapo/Android-Password-Store), mit der man die Passwörter auch auf mobilen Geräten nutzen kann. Ob man seine Passörter und seinen gpg-Key allerdings auf seinem Telefon haben möchte, muss jeder für sich entscheiden.