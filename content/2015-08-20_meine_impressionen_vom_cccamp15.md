Title: Meine Impressionen vom #cccamp15
Date: 2015-08-20 0:08
Category: ccc
Tags: cccamp, camp, ccc, fotos
Slug: 2015/meine_impressionen_vom_cccamp15
Status: published

Das Chaos Communication Camp 2015 ist ja nun auch schon seit ein paar Tagen zu Ende. Inzwischen ist es mir auch gelungen, meine Bilder zu sichten und ein paar schöne raus zu suchen.

Das Camp war definitiv großartig! Ich kann es nicht in Worte fassen wie toll es war!

Ich habe eine Menge gelernt, andere Chaoten wieder getroffen und ganz viel Spaß gehabt.

Mit den Bildern versuche ich ein wenig den Geist des Camps zu vermitteln. Ob mir das auch nur im Ansatz gelingt, kann ich nicht abschätzen...

[<img src="{filename}/images/2015/m_cccamp15_00.jpg">]({filename}/images/2015/cccamp15_00.jpg)
Extremlöten mit zwei Lötkolben gleichzeitig

[<img src="{filename}/images/2015/m_cccamp15_01.jpg">]({filename}/images/2015/cccamp15_01.jpg)
verschwommene Lichtinstallation auf dem Camp

[<img src="{filename}/images/2015/m_cccamp15_02.jpg">]({filename}/images/2015/cccamp15_02.jpg)
farbig beleuchteter Ringofen

[<img src="{filename}/images/2015/m_cccamp15_03.jpg">]({filename}/images/2015/cccamp15_03.jpg)
Die C-Base ist gelandet

[<img src="{filename}/images/2015/m_cccamp15_04.jpg">]({filename}/images/2015/cccamp15_04.jpg)
Seifenblasen in der Nacht

[<img src="{filename}/images/2015/m_cccamp15_05.jpg">]({filename}/images/2015/cccamp15_05.jpg)
*gespenstischer* Brennofen

[<img src="{filename}/images/2015/m_cccamp15_06.jpg">]({filename}/images/2015/cccamp15_06.jpg)
Funkenflug, wie tausend Sternschnuppen

[<img src="{filename}/images/2015/m_cccamp15_07.jpg">]({filename}/images/2015/cccamp15_07.jpg)
Muss baggern! Immer tiefer baggern!

[<img src="{filename}/images/2015/m_cccamp15_08.jpg">]({filename}/images/2015/cccamp15_08.jpg)
Lichter aus der Ferne

[<img src="{filename}/images/2015/m_cccamp15_09.jpg">]({filename}/images/2015/cccamp15_09.jpg)
Das Camp bei Nacht - Teil 1

[<img src="{filename}/images/2015/m_cccamp15_10.jpg">]({filename}/images/2015/cccamp15_10.jpg)
Das Camp bei Nacht - Teil 2

[<img src="{filename}/images/2015/m_cccamp15_11.jpg">]({filename}/images/2015/cccamp15_11.jpg)
Bunter Stand am Marktplatz

[<img src="{filename}/images/2015/m_cccamp15_12.jpg">]({filename}/images/2015/cccamp15_12.jpg)
Timewarp!
