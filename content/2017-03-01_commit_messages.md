Title: git - wie ich meine Commit-Messages schreibe
Date: 2017-03-01 08:11
Category: frickeln
Tags: linux. programming, git, development, commit
Slug: 2017/commit_messages
Status: published
Author: telegnom

Inspiriert duch einen Freund habe ich angefangen mein git commit messages mit einem System zu taggen. Dadurch ist schnell und einfach zu erkennen, was sich in dem Commit geändert hat. Das System hilft mir auch dabei kleine Commits mit einem eindeutigem Inhalt zu erstellen. Ich habe immer die Neigung alles in einem Schwung zu ändern und dann alle Änderung in einem Schwung zu commiten, was der Übersichtlichkeit in größeren Projekten eher abträglich ist.

generelle Stuktur: `[?] Message`

**?** kann dabei sein:

**F**: Feature 

**B**: BugFix 

**R**: Refactoring ((almost) no functional change) 

**M**: Meta (version bumb, release, tags) 

**D**: Documentation (e.g. release notes, installation guide) 

**G**: Graphics 

**T**: Tests 

**Y**: Merge Changes

**S**: CodeStyle *without* functional effect (e.g. linter code) 

### Extras für gitlab

Da ich viel im [chaos.expert](https://chaos.expert) Gitlab arbeite, habe ich mir für einige Funktionen eine erweiterte Syntax überlegt, z.B. um Issues automatisch zu schließen. Dabei packe ich die Meta-Informationen zur Issue, abgetrennt durch ein Minuszeichen, an das Ende der Commit-Message.

#### Close Issue:

`[?] Message - #issue close`