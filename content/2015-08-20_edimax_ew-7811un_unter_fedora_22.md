Title: Edimax EW-7811Un unter Fedora 22
Date: 2015-08-20 11:18
Category: linux
Tags: linux, fedora, treiber, wlan, schmerzen
Slug: 2015/edimax_ew-7811un_unter_fedora_22
Status: published

Seit einigen Tagen steht mein Rechner so, dass ich unmöglich ein LAN-Kabel dauerhaft
dorthin bekomme. Klar, ich könnte mich durch drei Wände bohren, aber dazu habe ich auch
keine Lust, zumal es ja dieses *Wlan* gibt, dass ja auch schon recht hübsche Übertragungsraten
ermöglicht.

Einen wirklich schnellen Wlan-Stick, der am Besten den [Standard AC][wlanac] spricht und
unter Linux läuft, muss ich mir noch suchen. Vorerst muss ein Edimax EW-7811Un
herhalten. Von den Dingern habe ich ein paar, da diese Sticks sich an den
Raspberry Pis bewährt haben.

Leider liefen die Sticks an die Pis problemloser als an meiner Fedora22 Kiste.
Nach einigem Suchen habe ich dann bei der Suchmaschine des geringsten Misstrauens
ein vielversprechendes [GitHub Repro][githubrepro] gefunden, in dem ein Patch für die Realtek Treiber
angeboten wird.

Für die Installation werden folgende Pakete benötigt:
 - git
 - kernel-devel
 - gcc
 - dkms
Sollten diese noch nicht vorhanden sein, lassen sie sich mit dem Befehl `dnf install git kernel-devel gcc dkms`
installieren. Damit sind die Vorbereitungen auch schon abgeschlossen und die eigentliche
Installation kann beginnen:

Clonen (herunterladen) des kompletten Git-Repositories:
``` sh
git clone https://github.com/pvaret/rtl8192cu-fixes.git
```

DKMS mit den Quellen für die neuen Module bekannt machen:
``` sh
sudo dkms add ./rtl8192cu-fixes
```

Module bauen und installiern (lassen):
``` sh
sudo dkms install 8192cu/1.10
```

Mappings der Kernelmodule erneuern:
``` sh
sudo depmod -a
```

Das mitgelieferte Kernelmodul *blackllisten*:
``` sh
sudo cp ./rtl8192cu-fixes/blacklist-native-rtl8192.conf /etc/modprobe.d
```

Abschließend den Rechner einmal durchbooten und schon funktioniert der Wlan-Stick
wieder tadellos!

[wlanac]: https://en.wikipedia.org/wiki/IEEE_802.11ac "IEEE 802.11ac"
[githubrepro]: https://github.com/pvaret/rtl8192cu-fixes "GitHub Repro von Pvaret"
