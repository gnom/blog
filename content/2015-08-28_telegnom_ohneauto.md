Title: telegnom.ohneAuto()
Date: 2015-09-03 19:25
Category: fahrrad
Tags: fahrrad, auto, pendeln, alltag
Slug: 2015/telegnom_ohneauto
Status: published

## Der Abschied vom Auto

Ende Juli hat der TÜV mir mein mehr oder minder geliebten Corsa stillgelegt. Es gab nicht einfach nur keine neue Plakette, sondern der TÜV hat auch gleich die alte runter gekratzt. In einer Werkstatt hätte mich die Reparatur sicher mehrere tausend Euro gekostet und in Eigenarbeit nur ein paar Hundert Euro, aber auch mindestens zwei oder drei Wochenenden Arbeit. Beide Optionen waren es mir bei einem 14 Jahre alten Auto nicht wert. Daher habe ich mich, gar nicht so schweren Herzens, von dem Auto getrennt. Das Auto los zu werden war gar nicht so schwer. Mit ein paar schlechten Bildern auf einer der gängigen Gebrauchtwagenportale eingestellt und schon nach wenigen **Sekunden** klingelte mein Telefon. Da der erste Ankäufer direkt bereit war die geforderte Summe zu zahlen, habe ich das Auto auch direkt an ihn verkauft. In Summe hat der Verkauf des alten Autos (Bilder machen, Annonce erstellen, *Probefahrt* und die Abmeldung) keine zwei Stunden gedauert. Ich war ein wenig verblüfft wie problemlos sich ein, zwar fahrbereites, aber nicht verkehrssicheres Auto verkaufen lässt.

## Auto? Brauche ich das wirklich?

Mit dem plötzlichen Ende meines Auto stand ich auf einmal auch vor einer neuen Situation. Bisher konnte ich, zu jeder Tages. und Nachtzeit, einfach in meine Karre springen und sonst wo hin fahren. Das geht nun nicht mehr. Die ersten Tage habe ich, da ich keine andere Wahl hatte, mit öffentlichen Verkehrsmitteln und dem Fahrrad überbrückt.

Relativ kurzfristig musste ich mich auch mit der Frage befassen, wie meine Mobilität in Zukunft denn aussehen soll. Dabei kommen ein paar objektive Fakten zum Tragen, aber auch einige subjektive oder gar emotionale Gesichtspunkte.

### Die harten Fakten

#### Der Weg zur Arbeit

Mit dem Auto sind es 18km von Haustür zu Haustür. Dafür benötige ich, je nach Tageszeit zwischen 20 und 45 Minuten. Auf der Strecke staut es sich im Berufsverkehr oft an zwei Stellen.

Mit dem Fahrrad sind knapp 15 km, für die ich auf dem Hinweg ca. 40 Minuten benötige. Der Rückweg dauert knapp eine Stunde, da es dann leider bergauf geht.

Meine Arbeitsstelle ist für Frankfurter Verhältnisse nur mäßig gut mit öffentlichen Verkehrsmitteln erreichbar. Ab meiner Wohnung muss ich 3km gehen, oder 20 Minuten mit dem Bus zur U-Bahn fahren, gefolgt von einer 18 minütigen U-Bahnfahrt. Ab der U-Bahnhaltestelle habe ich die Wahl entweder 15  Minuten zur Firma zu gehen oder zwei Haltestellen weiter zu fahren und dann mit dem Bus zur Arbeit zu fahren. Das dauert zusammen in der Regel so lange, dass Gehen die schnellere Option ist. Aber das ist nicht das gelbe vom Ei, da ich so knapp eine Stunde unterwegs bin.

Bleibt noch die Kombination aus Fahrrad und U-Bahn. Mit dem Fahrrad sind es drei Kilometer von der Haustür bis zur U-Bahnhaltestelle und dann ein Kilometer von der U-Bahn zur Arbeit. Die Mitnahme des Fahrrads ist bei der VGF glücklicherweise "problemlos" zu allen Tages und Nachtzeiten möglich. Für die Strecke von der Wohnung zur U-Bahn benötige ich knapp 10 Minuten und von der U-Bahn zur Firma sind es dann noch drei oder vier Minuten. In Summe bin ich also etwas mehr als eine halbe Stunde unterwegs. Die Stecken habe ich zeitlich so im Griff, dass ich keine nennenswerten Wartezeiten an den Haltestellen habe.

Von den Alternativen zum Auto ist die Kombination aus Fahrrad und Bahn (Bike & Bahn) in meinen Augen die praktikabelste. Also bleiben hier Auto oder Bahn & Bike übrig.

### Freizeit

In meiner Freizeit bin ich bisher auch eher wenig Auto gefahren. Für Fernreisen (alles weiter als Frankfurt) habe ich schon immer die Bahn bevorzugt. Bahnreisen finde ich angenehmer. Mit dem Auto bin ich zwar flexibler und nicht an den Fahrplan der Bahn gebunden, aber ich komme deutlich entspannter an. Während einer Bahnfahrt kann ich etwas lesen, programmieren oder $Dinge tun. Im Auto geht das nicht. Selbst als Beifahrer kann ich im Auto nicht viel mehr als Podcasts hören, oder schlafen. Lesen kann ich im fahrenden Auto überhaupt nicht. In Züge habe ich damit interessanter Weise überhaupt keine Probleme.

Allerdings habe ich einen Termin im Monat, den Hobbybrauerstammtisch Rhein-Main, der mit öffentlichen Verkehrsmitteln oder dem Fahrrad (mit dem Gepäck was ich da immer dabei habe) nicht wirklich erreichbar ist. Für diesen einen Fall kann ich mir entweder ein Auto bei einem der Carsharing-Portale mieten oder, wenn sie es nicht selber brauchen, auf das Auto der Eltern zurück greifen.

Alle sonstigen Aktivitäten finden in Bad Homburg, Frankfurt oder dem nahmen Umland statt und sind daher sehr gut mit Bike & Bahn erreichbar.

Daher sehe ich hier absolut keinen Grund wieso ich ein Auto bräuchte. Damit geht der Punkt klar an Bike & Bahn.

### Wenn mal "was" sein sollte

Wenn ich mal dringend zum Arzt muss wäre ein Auto praktisch, besonders wenn ich mich so schlapp und elend fühle, dass ich es nicht mehr mit dem Fahrrad schaffe. Aber ist es in so einer Situation noch angebracht mit dem Auto zu fahren? Ich glaube nicht. In der Praxis würde ich es aber vermutlich trotzdem machen. Ohne Fahrrad würde ich mir vermutlich ein Taxi nehmen, was bei der Entfernung von meiner Wohnung zum Hausarzt vielleicht einen Zehner kostet, also durchaus leistbar ist.

Da es unvernünftig wäre selber mit dem Auto zu fahren, wenn man sich auch nicht mehr im Sattel halten kann werde ich diesen Punkt nicht mit bewerten. Dazu kommt, dass mein Hausarzt mich zu letzt vor zwei oder drei Jahren gesehen hat, da ich eine eher robuste Gesundheit habe, die mal auch groben Unfug verzeiht.

### Einkaufen

Den täglichen Kram bekomme ich in dem Supermarkt, genau gegenüber meiner Wohnung. Das sind zu Fuß, aber der Wohnungstür keine 60 Sekunden bis rein. Daher brauche ich für den Einkauf von Lebensmitteln und Dingen die es im Supermarkt gibt kein Auto. Es wäre sogar eher hinderlich (in dieser speziellen Situation).

Für mittelgroße Dinge wie eine neue Gasflasche für den Grill oder zwei Kisten Getränke vom Getränkehändler sind tatsächlich problematisch. Das war auch mit einem Kleinwagen kein großes Problem, aber auf dem Fahrrad ist so etwas nicht zu transportieren. Hier muss ich mir entweder das Auto der Eltern leihen oder ich lege mir noch einen Transportanhänger für das Fahrrad zu.

Schwere und sperrige Dinge aus dem Möbel- oder Baummarkt konnte ich auch in meinem Kleinwagen nicht transportieren. Daher musste ich hier auch in der Vergangenheit schon Freunde mit größeren Autos um Fahrdienste bitten. Hier ändert sich also nichts.

In Summe kann ich hier auf ein eigenes Auto recht gut verzichten. Die Frage für "mittlere" Lasten muss ich noch entscheiden, vermutlich wäre es aber dann im Zweifel dann auch vernünftiger sich an einem oder zwei Tagen im Monat mal ein Carsharing Auto zu mieten als dafür einen eigenen Wagen anzuschaffen.

## Auto oder Bike & Bahn

Kurz und knapp: Bike and Bahn

Aber ein wenig muss ich noch optimieren, wie im nächsten Abschnitt zu lesen ist. Die tägliche Mitnahme des Trekkingrads in der U-Bahn ist nicht immer unproblematisch, wenn auch erlaubt.

## Bike & Bahn

Wie im weiter oben geschrieben habe, ist die Fahrradmitnahme bei der VGF überhaupt kein Problem. Jedenfalls in der Theorie. Praktisch nicht immer, da die U-Bahn in den Stoßzeiten so voll ist, dass ich schon mehrfach Probleme hatte mit dem Fahrrad die U-Bahn an der gewünschten Haltestelle zu verlassen. Beim Einsteigen habe ich keine Probleme mit dem Fahrrad, da ich an einer der Endhaltestellen der Linie einsteige. Daher muss hier noch eine Optimierung her. Mein aktuelles Fahrrad ist einfach zu groß und zu sperrig für den täglichen Transport in der U-Bahn.

### Ein Faltrad soll es sein

Aktuell umgehe ich das Problem in dem ich sehr früh zur Arbeit aufbreche, was für mich auf Dauer aber keine Lösung ist. Daher war ich auf der Suche nach einem Faltrad, dass für den Transport in öffentlichen Verkehrsmitteln besser geeignet ist als mein Trekkingrad.

Schlussendlich habe ich mich hier für ein Brompton entscheiden, weil es ein akzeptable Fahreigenschaften mit einem sehr kompakten Faltmaß verbindet. Leider haben die Räder aus London eine nicht zu verachtende Lieferzeit, so dass ich es leider noch nicht habe :(


<!--[^bnb]: Bahn & Bike - Mit dem Fahrrad zur Bahn, Mitnahme des Fahrrads in der Bahn und mit dem Fahrrad zum Ziel-->
