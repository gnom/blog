Title: DNF: Update von Paketen verhindern
Date: 2016-10-01 17:08
Category: linux
Tags: linux. fedora, dnf, update
Slug: 2016/dnf_update_von_paketen_verhinden
Status: published

Hin und wieder hat man ja das Problem, dass man nicht möchte, dass ein bestimmtes Paket aktualisiert wird. Weil zum Beispiel neuere Versionen einen Bug enthalten, der das Paket für einen unbrauchbar macht.

Um das automatische Update eines Pakets zu verhindern kann man in der Konfigurationsdatei von dnf folgendes eintragen:

```exclude=eris```

Dies verhindert das automatische Update des Pakets `eris`

Wenn man eine ganze Gruppe von Paketen ausschließen möchte, kann man Wildcards verwenden:

```exclude=packet*```

Hiermit werden alle Pakete die mit `packet` beginnen ausgeschlossen.

Sollen mehrere Pakete ausgeschlossen werden, kann man die Namen der Pakete einfach hintereinander auflisten, getrennt durch ein Leerzeichen:

```exclude=foo bar baz```

Dieses Beispiel würde die Pakete `foo`, `bar`, und `baz` vom automatischen Update ausschließen.

Einzutragen ist das ganze in der Datei `/etc/dnf/dnf.conf`. Beim nächsten Update werden die angegebenen Pakete dann nicht mehr aktualisiert.