Title: Fork me on chaos.expert ribbon
Date: 2017-03-01 18:37
Category: frickeln
Tags: linux. programming, git, development, merch, ribbon, fork me
Slug: 2017/fork_me
Status: published

Vielen ist sicherlich der "fork me on github.com" Banner bekannt, den sich viele Leute in ihre Homepage, Blogs, whatever eingebunden haben. Dank der Arbeit von [Simon Whitakers](https://twitter.com/s1mn) gibt es eine Implementierung des Bands in reinem CSS, ohne Bilder, nur Text :) Das Bändchen habe ich mit geschnappt und so umgebaut, dass es nun für [chaos.expert](https://chaos.expert/chaos-expert/fork-ribbon) verwendet werden kann. 

### Beispiel

![screenshot]({filename}/images/2017/fork_me_ribbon_01.png "Screenshot of Fork me on chaos.expert in action")

Das Bild steht, denke ich, für sich.

### Installation und Integration

#### Installation mit Bower

Das Band kann über bower, mit dem Befehl `bower install chaos-expert-fork-ribbon` installiert werden. 

#### Manuelle Installation

Für die manuelle Installation ist es am einfachsten, wenn man sich das gesammte [Git-Repo](https://chaos.expert/chaos-expert/fork-ribbon) klont bzw. herunterlädt. Aus dem Repo werden allerdings nur die beiden Dateien `chaos-expert-fork-ribbon.css` und `chaos-expert-fork-ribbon.ie.css` benötigt.

#### Integration

Zur Integration müssen die beiden `.css` Dateien im `<head>` Abschnitt der HTML-Seite verlinkt werden. Dazu kann man sich einfach das folgende Code-Schnippselchen kopieren:
```
<link rel="stylesheet" href="<path>/<to>/<your>/<css>/chaos-expert-fork-ribbon.css">
<!--[if lt IE 9]>
    <link rel="stylesheet" href="<path>/<to>/<your>/<css>/chaos-expert-fork-ribbon.ie.css">
<![endif]-->
```

Anschließend kann man den folgenden Schnippsel im `<body>` der Seite einfügen. Die Stelle an der die Zeile einbgefügt wird ist egal, solange es zwischen `<body>` und `</body>` erfolgt.

```
<a class="chaos-expert-fork-ribbon" href="http://chaos.expert/telegnom" title="Fork me on chaos.expert">
    Fork me on chaos.expert
</a>
```

Natürlich muss der Link entsprechend auf die eigene Übersichtsseite im chaos.expert gitlab angepasst werden, du willst ja sicherlich nicht auf meine Werke verlinken ;)

Eine ausführliche Installationsanleitung findet sich in der [Readme](https://chaos.expert/chaos-expert/fork-ribbon/blob/ceRibbon/README.md) des Repos.

### Bugs und Wünsche

Fehler und Verbesserungsverschläge postest du am besten im Issue-Tracker des Projekts unter [chaos.expert/chaos-expert/fork-ribbon/issues](https://chaos.expert/chaos-expert/fork-ribbon/issues)