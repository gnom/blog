Title: ESP8266 Verkabelung - Modell ESP12
Date: 2015-09-08 06:40
Category: frickeln
Tags: esp8266, µController, flashen, nodemcu
Slug: 2015/esp8266_verkabelung-modell_esp12
Status: draft



Das ich mir nie merken kann, wie ein ESP12 *minimal* verkabelt werden muss, habe ich mal zwei Zeichnungen gemacht, wie es sein muss. 

## Verkabelung zum Flashen der Firmware (nodemcu)

![Verkabelung zum Flashen der Firmware (nodemcu)]({filename}/images/2015/esp12_flash_fw_steckplatine.jpg "Verkabelung zum Flashen der Firmware")

## Verkabelung im *normalen* Betrieb

![Verkabelung im normalen Betrieb]({filename}/images/2015/esp12_run_steckplatine.jpg "Verkabelung im normalen Betrieb")

Die Widerstände sind allesamt 10k.
