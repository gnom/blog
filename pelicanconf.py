#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'telegnom'
SITENAME = 'telegnom.org'
SITEURL = 'https://telegnom.org'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'de'

# Plugins
PLUGIN_PATHS = ["plugins/"]
PLUGINS = ['tag_cloud']
MD_EXTENSIONS = ['codehilite(css_class=highlight)', 'extra', 'toc', 'headerid']

CATEGORY_URL = 'category/{slug}/'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'
CATEGORIES_URL = 'categories/'
CATEGORIES_SAVE_AS = 'categories/index.html'
DIRECT_TEMPLATES = [
    'index',
    'categories',
]

#disclaimer and license
DISCLAIMER = 'Powered by love &amp; rainbow sparkles.'
COPYRIGHT = 'This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a>.'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Theme
THEME = './themes/eevee'
THEME_PRIMARY = 'purple'
THEME_ACCENT = 'deep_orange'

# Menu
MENUITEMS = (('<i class="fa fa-linux aria-hidden="true"></i> Linux', '/category/linux/'),
            ('<i class="fa fa-rocket aria-hidden="true"></i> CCC', '/category/ccc'),
            ('<i class="fa fa-bicycle aria-hidden="true"></i> Fahrrad', '/category/fahrrad/'),
            ('<i class="fa fa-utensils aria-hidden="true"></i> Kochen', '/category/kochen/'),
            ('<i class="fa fa-wrench aria-hidden="true"></i> Make', '/category/frickeln'))

# Blogroll
LINKS = (('<i class="fa fa-gitlab aria-hidden="true"></i> chaos.expert', 'https://chaos.expert'),
         ('<i class="fa fa-rocket aria-hidden="true"></i> ccc.de', 'https://ccc.de'),
         ('<i class="fa fa-rocket aria-hidden="true"></i> ccc-ffm.de', 'https://ccc-ffm.de'),
         ('<i class="fa fa-rocket aria-hidden="true"></i> chaospott.de', 'https://chaospott.de')
        )

# Social widget
SOCIAL = (('<i class="fa fa-twitter aria-hidden="true"></i> twitter', 'https://twitter.com/telegnom'),
          ('<i class="fa fa-random aria-hidden="true"></i>  soup.io', 'https://telegnom.soup.io'),)

DEFAULT_PAGINATION = 10

USE_OPEN_GRAPH = True
USE_TWITTER_CARDS = True
TWITTER_USERNAME = 'telegnom'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
